create table `group`
(
	id int auto_increment
		primary key,
	name varchar(50) not null,
	parent_id int null,
	visible tinyint(1) default '0' null,
	constraint group_id_uindex
		unique (id),
	constraint group_group_id_fk
		foreign key (parent_id) references `group` (id)
)
;

create index group_group_id_fk
	on `group` (parent_id)
;

create table relation_rigth_to_group
(
	id int auto_increment
		primary key,
	id_right int default '1' not null,
	id_group int default '1' not null,
	constraint relation_rigth_to_group_id_uindex
		unique (id),
	constraint relation_rigth_to_group_group_id_fk
		foreign key (id_group) references `group` (id)
)
;

create index relation_rigth_to_group_group_id_fk
	on relation_rigth_to_group (id_group)
;

create index relation_rigth_to_group_right_id_fk
	on relation_rigth_to_group (id_right)
;

create table relation_user_to_group
(
	id_user_to_group int auto_increment
		primary key,
	id_user int default '1' not null,
	id_group int default '1' not null,
	constraint relation_user_to_group_id_user_to_group_uindex
		unique (id_user_to_group),
	constraint relation_user_to_group_group_id_fk
		foreign key (id_group) references `group` (id)
)
;

create index relation_user_to_group_group_id_fk
	on relation_user_to_group (id_group)
;

create index relation_user_to_group_user_id_fk
	on relation_user_to_group (id_user)
;

create table rights
(
	id int auto_increment
		primary key,
	name varchar(50) null,
	constraint rights_id_uindex
		unique (id)
)
;

alter table relation_rigth_to_group
	add constraint relation_rigth_to_group_right_id_fk
		foreign key (id_right) references rights (id)
;

create table user
(
	id int auto_increment
		primary key,
	login varchar(50) not null,
	name varchar(50) null,
	email varchar(100) null,
	constraint user_id_uindex
		unique (id)
)
;

alter table relation_user_to_group
	add constraint relation_user_to_group_user_id_fk
		foreign key (id_user) references user (id)
;

