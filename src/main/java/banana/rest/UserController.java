package banana.rest;

import banana.dao.UserDAO;
import banana.dto.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import banana.pojo.BoostUserEntity;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.springframework.web.bind.annotation.RequestMethod.*;
import static rest.RestURIConstants.*;


@Controller
@RequestMapping(value = "/user",
		produces = "application/json",
		consumes = "application/json",
		headers = "Accept=*/*")
@EnableWebMvc
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserDAO userDAO;

	@RequestMapping(
			value = USER_CREATE,
			method = POST)
	public @ResponseBody
	User createUser(@RequestBody User userToBeCreated) {
		logger.info("createUser");

		User user = new User();
		user.setEmail(userToBeCreated.getEmail());
		user.setLogin(userToBeCreated.getLogin());
		user.setName(userToBeCreated.getName());

		BoostUserEntity boostUserEntity = new BoostUserEntity();
		boostUserEntity.setLogin(user.getLogin());
		boostUserEntity.setEmail(user.getEmail());
		boostUserEntity.setName(user.getName());


		userDAO.save(boostUserEntity);
		return user;
	}

	@RequestMapping(
			value = USER_EDIT,
			method = POST)
	public @ResponseBody
	User editUser(@RequestParam Integer id, @RequestBody User userToBeEdited) {
		logger.info("updateUser");

		User updatedUser = new User();
		updatedUser.setEmail(userToBeEdited.getEmail());
		updatedUser.setLogin(userToBeEdited.getLogin());
		updatedUser.setName(userToBeEdited.getName());

		BoostUserEntity boostUserEntity = userDAO.get(id.longValue());
		boostUserEntity.setLogin(userToBeEdited.getLogin());
		boostUserEntity.setEmail(userToBeEdited.getEmail());
		boostUserEntity.setName(userToBeEdited.getName());

		userDAO.update(boostUserEntity);
		return updatedUser;
	}

	@RequestMapping(value = USER_DELETE, method = GET)
	public @ResponseBody
	User deleteUser(@RequestParam("id") Integer userToBeDeleted) {
		logger.info("deleteUser");
		BoostUserEntity boostUserEntity = userDAO.get(userToBeDeleted.longValue());
		userDAO.delete(boostUserEntity);
		return new User();
	}

}