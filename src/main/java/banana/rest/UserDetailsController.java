package banana.rest;

import banana.dto.UserDetails;
import banana.service.UserDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.LinkedList;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static rest.RestURIConstants.USER_DETAIL_BY_ID;
import static rest.RestURIConstants.USER_DETAIL_BY_ID_LIST;

@Controller
@RequestMapping(
		produces = "application/json",
		consumes = "application/json",
		headers = "Accept=*/*")
@EnableWebMvc
public class UserDetailsController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserDetailService userDetailService;

	@RequestMapping(
			value = USER_DETAIL_BY_ID,
			method = GET)
	public @ResponseBody
	UserDetails readUserDetail(@RequestParam Long id) {
		logger.info("readUserDetail");

		return userDetailService.getUserDetails(id);

	}

	@RequestMapping(
			value = USER_DETAIL_BY_ID_LIST,
			method = GET)
	public @ResponseBody
	List<UserDetails> readUserDetailList(@RequestParam(name = "id") Long[] idList) {
		logger.info("readUserDetailList");

		List<UserDetails> userDetails = new LinkedList<>();
		for (Long id : idList) {
			userDetails.add(userDetailService.getUserDetails(id));
		}
		return userDetails;

	}

}
