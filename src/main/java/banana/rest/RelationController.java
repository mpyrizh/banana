package banana.rest;

import banana.dao.RelationDAO;
import banana.dto.ResponseData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static banana.dto.OperationEnum.*;
import static banana.dto.StatusEnum.*;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static rest.RestURIConstants.*;

@Controller
@RequestMapping(value = "/relation",
		produces = "application/json",
		consumes = "application/json",
		headers = "Accept=*/*")
@EnableWebMvc
public class RelationController {

	private static final Logger logger = LoggerFactory.getLogger(RelationController.class);

	@Autowired
	private RelationDAO relationDAO;

	@RequestMapping(value = RELATION_USER_TO_GROUP_ADD, method = GET)
	public @ResponseBody
	ResponseData relationAdd(@RequestParam("user_id") Integer userId,
							@RequestParam("group_id") Integer groupId) {
		logger.info("relationAdd");

		relationDAO.addUserToGroupRelation(userId.longValue(), groupId.longValue());

		ResponseData response = new ResponseData();
		response.setOperationStatus(RELATION_USER_TO_GROUP);
		response.setStatusEnum(SUCCESS);
		return response;
	}

	@RequestMapping(value = RELATION_USER_TO_GROUP_REMOVE, method = GET)
	public @ResponseBody
	ResponseData relationRemove(@RequestParam("user_id") Integer userId,
							@RequestParam("group_id") Integer groupId) {
		logger.info("relationRemove");

		relationDAO.removeUserToGroupRelation(userId.longValue(), groupId.longValue());

		ResponseData response = new ResponseData();
		response.setOperationStatus(RELATION_USER_TO_GROUP_DELETE);
		response.setStatusEnum(SUCCESS);
		return response;
	}

}
