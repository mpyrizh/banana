package banana.pojo;

import javax.persistence.*;

@Entity
@Table(name = "relation_user_to_group", schema = "boost_task", catalog = "")
public class BoostRelationUserToGroupRelation {

	private Long id;
	private Long groupByIdGroup;
	private Long userByIdUser;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_group", insertable = false, updatable = false)
	public Long getGroupByIdGroup() {
		return groupByIdGroup;
	}

	public void setGroupByIdGroup(Long groupByIdGroup) {
		this.groupByIdGroup = groupByIdGroup;
	}

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_user", insertable = false, updatable = false)
	public Long getUserByIdUser() {
		return userByIdUser;
	}

	public void setUserByIdUser(Long userByIdUser) {
		this.userByIdUser = userByIdUser;
	}
}
