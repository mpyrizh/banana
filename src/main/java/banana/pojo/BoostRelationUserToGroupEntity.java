package banana.pojo;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class BoostRelationUserToGroupEntity {
	private BoostGroupEntity groupByIdGroup;

	@ManyToOne
	@JoinColumn(table = "group", name = "id_group", referencedColumnName = "id", nullable = false)
	public BoostGroupEntity getGroupByIdGroup() {
		return groupByIdGroup;
	}

	public void setGroupByIdGroup(BoostGroupEntity groupByIdGroup) {
		this.groupByIdGroup = groupByIdGroup;
	}

	private BoostUserEntity userByIdUser;

	@ManyToOne
	@JoinColumn(table = "user", name = "id_user", referencedColumnName = "id", nullable = false)
	public BoostUserEntity getUserByIdUser() {
		return userByIdUser;
	}

	public void setUserByIdUser(BoostUserEntity userByIdUser) {
		this.userByIdUser = userByIdUser;
	}
}
