package banana.pojo;

import javax.persistence.*;

@Entity
@Table(name = "relation_rigth_to_group", schema = "boost_task", catalog = "")
public class BoostRelationRigthToGroupEntity {
	private Integer id;
	private Integer groupId;
	private Integer rightId;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_group", insertable = false, updatable = false)
	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_right", insertable = false, updatable = false)
	public Integer getRightId() {
		return rightId;
	}

	public void setRightId(Integer rightId) {
		this.rightId = rightId;
	}
}
