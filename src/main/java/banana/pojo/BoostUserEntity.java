package banana.pojo;

import javax.persistence.*;

@Entity
@Table(name = "user", schema = "boost_task", catalog = "")
public class BoostUserEntity {
	private Long id;
	private String login;
	private String name;
	private String email;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Basic
	@Column(name = "login", nullable = false, length = 50)
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Basic
	@Column(name = "name", nullable = true, length = 50)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Basic
	@Column(name = "email", nullable = true, length = 100)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		BoostUserEntity that = (BoostUserEntity) o;

		if (id != null ? !id.equals(that.id) : that.id != null) return false;
		if (login != null ? !login.equals(that.login) : that.login != null) return false;
		if (name != null ? !name.equals(that.name) : that.name != null) return false;
		return email != null ? email.equals(that.email) : that.email == null;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (login != null ? login.hashCode() : 0);
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (email != null ? email.hashCode() : 0);
		return result;
	}
}
