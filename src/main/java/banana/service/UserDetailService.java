package banana.service;

import banana.dto.UserDetails;

public interface UserDetailService {

	public UserDetails getUserDetails(Long userId);

}
