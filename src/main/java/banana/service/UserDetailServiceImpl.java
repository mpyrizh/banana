package banana.service;

import banana.dao.RelationDAO;
import banana.dao.UserDAO;
import banana.dto.UserDetails;
import banana.pojo.BoostUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Service
public class UserDetailServiceImpl implements UserDetailService {

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private RelationDAO relationDAO;

	@Override
	public UserDetails getUserDetails(Long userId) {

		BoostUserEntity boostUserEntity = userDAO.get(userId);
		UserDetails userDetails = new UserDetails();
		userDetails.setEmail(boostUserEntity.getEmail());
		userDetails.setLogin(boostUserEntity.getLogin());
		userDetails.setName(boostUserEntity.getName());

		List<Long> groupIdForUser = relationDAO.selectAllGroupIdForUser(userId);

		List<Integer> integerList = new LinkedList<>();
		Set<Long> userGroupsIdSet = new HashSet<>();
		for (Long groupId : groupIdForUser) {
			integerList.addAll(relationDAO.selectAllGroupsUpToRootForGroupId(groupId));

		}
		Set<Integer> userRightsIdSet = relationDAO.readAllRightsWhereGroupInSet(integerList);
		Set<String> userRights = new HashSet<>();

		userRights.addAll(relationDAO.selectAllRightSetNames(userRightsIdSet));
		userDetails.setUserRights(userRights);

		userDetails.setId(userId);
		return userDetails;
	}
}
