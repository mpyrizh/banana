package banana.dao;

import banana.pojo.BoostRelationRigthToGroupEntity;
import banana.pojo.BoostRelationUserToGroupRelation;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Transactional
public class RelationDAOImpl implements RelationDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Long> selectAllGroupIdForUser(Long userId) {

		BoostRelationUserToGroupRelation relation = new BoostRelationUserToGroupRelation();

		Session session = sessionFactory.getCurrentSession();
		String hql = "select r.groupByIdGroup from BoostRelationUserToGroupRelation r where userByIdUser= :userByIdUser";

		Query query = session.createQuery(hql);
		query.setParameter("userByIdUser", userId);

		List<Long> relationList = query.list();
		return relationList;
	}

	@Override
	public List<Integer> selectAllGroupsUpToRootForGroupId(Long groupId) {

		Session session = sessionFactory.getCurrentSession();

		// run sql query
		String sql = "SELECT T2.id FROM ( SELECT "
				+ "@r AS _id,"
				+ "(SELECT @r \\:= parent_id FROM `group` WHERE id = _id ) AS parent_id,"
				+ "@l \\:= @l + 1 AS lvl "
				+ "FROM (SELECT @r \\:= :group_id_value, @l \\:= 0) vars,"
				+ " `group` h "
				+ "WHERE @r <> 0) T1 "
				+ "JOIN `group` T2 "
				+ "ON T1._id = T2.id "
				+ "WHERE visible =1 "
				+ " OR T2.id = :group_id_value "
				+ "ORDER BY T1.lvl DESC";
		NativeQuery query = session.createNativeQuery(sql);
		query.setParameter("group_id_value", groupId);
		List<Integer> results = query.list();
		return results;
	}

	@Override
	public Set<String> selectAllRightSetNames(Set<Integer> rightIdSet) {

		if (rightIdSet.isEmpty()) return new HashSet<>();
		Session session = sessionFactory.getCurrentSession();
		List<Integer> integerList = new ArrayList<>(rightIdSet);

		Query query = session.createQuery("select r.name FROM BoostRightEntity r WHERE r.id IN (:ids)");
		query.setParameterList("ids", integerList);
		List<String> rightList = query.getResultList();


		return new HashSet<>(rightList);
	}

	@Override
	public void addUserToGroupRelation(Long userId, Long groupId) {

		BoostRelationUserToGroupRelation relation = new BoostRelationUserToGroupRelation();

		relation.setGroupByIdGroup(groupId);
		relation.setUserByIdUser(userId);

		Session session = sessionFactory.getCurrentSession();
		session.save(relation);

	}

	@Override
	public void removeUserToGroupRelation(Long userId, Long groupId) {
		Session session = sessionFactory.getCurrentSession();

		String hql = "delete from BoostRelationUserToGroupRelation where groupByIdGroup= :groupByIdGroup AND userByIdUser= :userByIdUser";
		Query query = session.createQuery(hql);
		query.setParameter("groupByIdGroup", groupId);
		query.setParameter("userByIdUser", userId);
		query.executeUpdate();
	}

	@Override
	public Set<Integer> readAllRightsWhereGroupInSet(List<Integer> userGroupsIdSet) {
		List<Integer> ids = userGroupsIdSet;
		if (userGroupsIdSet.isEmpty()) return new HashSet<>();
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(" FROM BoostRelationRigthToGroupEntity r WHERE r.groupId IN ( :ids )");
		query.setParameterList("ids", userGroupsIdSet);
		List<BoostRelationRigthToGroupEntity> rightIdList = query.list();

		return rightIdList.stream().map(BoostRelationRigthToGroupEntity::getRightId).collect(Collectors.toSet());
	}
}
