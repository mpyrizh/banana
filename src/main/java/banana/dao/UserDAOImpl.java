package banana.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import banana.pojo.BoostUserEntity;

import java.util.List;

@Transactional
public class UserDAOImpl implements banana.dao.UserDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public BoostUserEntity get(Long uid) {
		Session session = sessionFactory.getCurrentSession();
		return (BoostUserEntity) session.get(BoostUserEntity.class, uid);
	}

	@Override
	public List<BoostUserEntity> getAll() {
		return null;
	}

	@Override
	public Long save(BoostUserEntity user) {
		Session session = sessionFactory.getCurrentSession();
		session.save(user);
		return user.getId();
	}

	@Override
	public BoostUserEntity update(BoostUserEntity user) {
		Session session = sessionFactory.getCurrentSession();
		session.update(user);
		return user;
	}

	@Override
	public void delete(BoostUserEntity user) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(user);
	}

}