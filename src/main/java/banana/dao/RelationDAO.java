package banana.dao;

import java.util.List;
import java.util.Set;

public interface RelationDAO {

	public List<Long> selectAllGroupIdForUser(Long userId);

	public List<Integer> selectAllGroupsUpToRootForGroupId(Long groupId);

	public Set<String> selectAllRightSetNames(Set<Integer> rightIdSet);

	public void addUserToGroupRelation(Long userId, Long groupId);

	public void removeUserToGroupRelation(Long userId, Long groupId);

	public Set<Integer> readAllRightsWhereGroupInSet(List<Integer> userGroupsIdSet);
}
