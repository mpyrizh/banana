package banana.dao;

import banana.pojo.BoostUserEntity;

import java.util.List;

public interface UserDAO {

	public BoostUserEntity get(Long uid);

	public List<BoostUserEntity> getAll();

	public Long save(BoostUserEntity user);

	public BoostUserEntity update(BoostUserEntity user);

	public void delete(BoostUserEntity user);

}