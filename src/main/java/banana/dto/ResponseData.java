package banana.dto;

public class ResponseData {

	private OperationEnum operationStatus;
	private StatusEnum statusEnum;

	public OperationEnum getOperationStatus() {
		return operationStatus;
	}

	public void setOperationStatus(OperationEnum operationStatus) {
		this.operationStatus = operationStatus;
	}

	public StatusEnum getStatusEnum() {
		return statusEnum;
	}

	public void setStatusEnum(StatusEnum statusEnum) {
		this.statusEnum = statusEnum;
	}
}
