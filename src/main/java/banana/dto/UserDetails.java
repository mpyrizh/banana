package banana.dto;

import java.util.Set;

public class UserDetails {

	private Long id;
	private String login;
	private String name;
	private String email;
	private Set<String> userRights;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<String> getUserRights() {
		return userRights;
	}

	public void setUserRights(Set<String> userRights) {
		this.userRights = userRights;
	}

}
