package banana.dto;

import java.io.Serializable;

public class User implements Serializable {

	private static final long serialVersionUID = -7788619177798333712L;

	private String login;
	private String name;
	private String email;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
