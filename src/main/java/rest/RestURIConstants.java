package rest;

public class RestURIConstants {

	public static final String USER_CREATE = "/create";
	public static final String USER_EDIT = "/edit";
	public static final String USER_DELETE = "/delete";

	public static final String RELATION_USER_TO_GROUP_ADD = "/user-to-group/create";
	public static final String RELATION_USER_TO_GROUP_REMOVE = "/user-to-group/unbind";

	public static final String USER_DETAIL_BY_ID = "/user-detail";
	public static final String USER_DETAIL_BY_ID_LIST = "/user-detail/list";
}
